///**
// * scripts.js
// * Contains Script for basic static website named "White Graphics"
// */
//
/*********************************************************
                        PRELOADER JS
*********************************************************/
$(window).on('load',function(){
    $("#preloader").delay(500).fadeOut('slow');
});

/*********************************************************
                        OWL CAROUSEL JS
*********************************************************/
$(document).ready(function(){
    $("#team-right").owlCarousel({
        items: 2,
        autoplay: true,
        margin: 20,
        loop: true,
        nav: true,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: false,
        navText: ['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>']
    });
});

$(document).ready(function(){
    $('#services-tabs').responsiveTabs({
//    startCollapsed: 'accordion'
        animation:'slide'
    });

});



$(document).ready(function(){
    $("#isotope-container").isotope({});
    
    $("#isotope-filters").on("click","button",function(){
        let filtervalue = $(this).attr("data-filter");
//        console.log(filtervalue);
        
        $("#isotope-container").isotope({
            filter:filtervalue
        });
        $("#isotope-filters").find('.active').removeClass('active');
    
    $(this).addClass('active');
})

});
    

$(document).ready(function(){
  $("#carousel-elements").owlCarousel({
      items:6,
      autoplay:true,
      margin:20,
      loop:true,
      nav:true,
      smartSpeed:1000,
      autoplayHoverPause:false,
      dots:false,
      navText:['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>'],
      responsive:{
          0:{
          items:2
        },
          480:{
              items:3
          },
          768:{
              items:6
          }
          
      }
  })
});

$(function () {
    new WOW().init();

    /*************************************************************************
                                WORK SECTION
    *************************************************************************/
    $('#work').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease-in-out', // CSS transition easing function

            opener: function (openerElement) {
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        }
    });

    
    $("a.smooth-scroll").click(function(event){
        event.preventDefault();
        
        var section = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(section).offset().top-69,
        }, 1250, "easeInOutExpo");
    });
    if($(this).scrollTop()>100){
            $("nav").addClass("wg-top-nav");
            $(".btn-back-to-top").fadeIn();
        }else{
            $("nav").removeClass("wg-top-nav");
            $(".btn-back-to-top").fadeOut();
        }
    /********************
            gallery
    *********************/
    $('.grid')({
  // options
  itemSelector: '.grid-item',
});
});


/*********************************************************
                    PORTFOLIO MAGNIFIC POPUP JS
*********************************************************/


$(document).ready(function(){
    $("#portfolio-wrapper").magnificPopup({
        delegate:'a',
        type:'image',
        gallery:{
            enabled:true
        },
        zoom:{
            enabled:true,
            duration:300,
            easing:'ease-in-out',
            opener:function(openerElement){
                return openerElement.is('img') ? openerElement:openerElement.find('img');
            }
        }
    })
});


/*********************************************************
                    MAPS JS
*********************************************************/

$(window).on('load',function(){
   
    var addressString = "301,Evergreen Chs.,Airoli,Maharashtra,India";
    var myLatLng = {
    lat:19.046605, 
    lng:73.011354
    };
    
    var myMap = new google.maps.Map(document.getElementById("map"),{
        zoom:13,
        center:myLatLng
    });
    
     var custimg = "img/map/";
    var marker = new google.maps.Marker({
        position:myLatLng,
        map:myMap,
        draggable: true,
        animation: google.maps.Animation.BOUNCE,
        title:"click to see",
        icon: custimg + 'logo.jpg'
    });
    
//    var image = 
    
    var infoWindow = new google.maps.InfoWindow({
       content:addressString 
    });
    marker.addListener('click',function(){
        infoWindow.open(myMap,marker);
    });
    
});


$(() => {
    $('#testimonials').owlCarousel({
        items: 3,
        margin: 30,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoPlayHoverPause: true,
        dots: true,
        dotsEach:  true,
        dotsData: true,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3    
            }
        }
    });
});
